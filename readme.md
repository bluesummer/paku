## Paku ##

**Paku** is an archiver/compressor for series of very similar lossless images.

Instead of using the normal compression algorithms/formats, Paku uses h264 video encoder to archive images. The archive output is essentially a lossless AVI video file (which can be played on media players). **ffmpeg and libx264** are used.

### Benefits ###

Tests have shown that if the number of similar images are large, Paku can compress better than 7zip solid mode.

Pack can extract the images directly into the lossy JPEG format. There is no need to extract intermediate temporary BMP file on to the disk for conversion.

It should be possible to develop image viewer plugins, which allows instant viewing of any images in the archive (which cannot be done for 7zip solid mode).

### Installation ###

Download the 7zip file and extract it into a folder. Run paku.exe to start the program.

### Limitations ###

Since the archive format is a video file, all the images need to be in the same dimension and format.

BMP file must be encoded using the same encoding. If one file uses different encoding, the compression will stop half way. Check that all BMP files have the same file size (usually means the same encoding is used), or convert them to PNG first then compress.

The image set must be groups of similar images, in lossless format (e.g. PNG or BMP), for the archiver to compress effectively. Trying to compress JPEG file would result in a larger-than-original archive.

There seems to be some glitches when manipulating Unicode file paths. If problem occurs, first try renaming the folder and file names to ascii format, and report the bug on Github.

Paku does not remember file names, nor the sub directory structure.

### Contributing ###

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## License

GNU General Public License (See license.txt)

