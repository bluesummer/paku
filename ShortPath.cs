﻿using System.Text;
using System.Runtime.InteropServices;

namespace paku
{
    class ShortPath
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetShortPathName(
            [MarshalAs(UnmanagedType.LPTStr)]string path,
            [MarshalAs(UnmanagedType.LPTStr)]StringBuilder short_path,
            int short_len
            );

        public static string GetShortPath(string name)
        {
            int lenght = 0;

            lenght = GetShortPathName(name, null, 0);
            if (lenght == 0)
            {
                return name;
            }
            StringBuilder short_name = new StringBuilder(lenght);
            lenght = GetShortPathName(name, short_name, lenght);
            if (lenght == 0)
            {
                return name;
            }
            return short_name.ToString();
        }
    }
}
