﻿using System;
using System.Text;
using System.Windows;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Controls;

namespace paku
{
    public partial class MainWindow : Window
    {
        private OpenFileDialog compressInputDlg;
        private FolderBrowserDialog compressInputFolderDlg;
        private SaveFileDialog compressOutputDlg;

        private OpenFileDialog decompressInputDlg;

        public MainWindow()
        {
            InitializeComponent();
            InitializeCompressTabWithWildcard();
            InitializeDecompressTab();
        }

        private void InitializeDecompressTab()
        {
            // Create OpenFileDialog 
            decompressInputDlg = new OpenFileDialog();

            // Set filter for file extension and default file extension 
            decompressInputDlg.DefaultExt = ".avi";
            decompressInputDlg.Filter = "AVI Files (*.h264)|*.h264";
        }

        private void InitializeCompressTab()
        {
            InitializeCompressTabCommon();
        }

        private void InitializeCompressTabWithWildcard()
        {
            // Dialog for selecting images to be compresses 
            compressInputFolderDlg = new FolderBrowserDialog();

            InitializeCompressTabCommon();
        }

        private void InitializeCompressTabCommon()
        {
            // Dialog for choosing where compressed file goes
            compressOutputDlg = new SaveFileDialog();

            compressOutputDlg.Filter = "avi files (*.h264)|*.h264";
            compressOutputDlg.FilterIndex = 2;
            compressOutputDlg.RestoreDirectory = true;
        }

        private void compressButton_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(compressInputPathTextBox.Text))
            {
                System.Windows.Forms.MessageBox.Show("Target folder not selected or does not exist.");
                return;
            }

            if (compressOutputDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string exePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "compress.bat");

                string inputAsciiPath = getAsciiPath(compressInputPathTextBox.Text);
                string format = ((ListBoxItem)compressInputFileFormatList.SelectedItem).Content.ToString();
                string inputArg = inputAsciiPath + "\\*." + format;

                Process p = new Process();
                p.StartInfo.FileName = exePath;
                p.StartInfo.Arguments = String.Format("\"{0}\" \"{1}\"", inputArg, compressOutputDlg.FileName);

                // Redirect the output stream of the child process.
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardError = true;
                StringBuilder consoleError = new StringBuilder();
                p.ErrorDataReceived += (innerSender, args) => consoleError.AppendLine(args.Data);

                p.Start();

                p.BeginErrorReadLine();
                p.WaitForExit();

                new LogForm(consoleError.ToString()).ShowDialog();

                // delete temp junction point if it is created
                if(Monitor.Core.Utilities.JunctionPoint.Exists(inputAsciiPath)) {
                    Monitor.Core.Utilities.JunctionPoint.Delete(inputAsciiPath);
                }
            }
        }

        private string getAsciiPath(string p)
        {
            if(isAscii(p)){
                return p;
            }

            // if not ascii
            string short_path = ShortPath.GetShortPath(p);
            if(short_path != p && isAscii(short_path)){
                return short_path; // if short_path is indeed short
            }

            // All else fail, create junction point
            string volumn = Path.GetPathRoot(p);
            string name = Path.GetRandomFileName();
            string junctionPath = volumn + name;
            Monitor.Core.Utilities.JunctionPoint.Create(junctionPath, p, false);
            return junctionPath;
        }

        private bool isAscii(string p)
        {
            foreach (char c in p)
            {
                if (c >= 128)
                {
                    return false;
                }
            }
            return true;
        }

        private void decompressLoadButton_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(decompressInputPathTextBox.Text))
            {
                System.Windows.Forms.MessageBox.Show("Archive file not selected or does not exist.");
                return;
            }

            // Select folder to extract to
            FolderBrowserDialog decompressOutputDlg = new FolderBrowserDialog();
            decompressOutputDlg.Description = "Select folder to decompress to:";
            decompressOutputDlg.SelectedPath = Path.GetDirectoryName(decompressInputPathTextBox.Text);
            
            if (decompressOutputDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string exePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "decompress.bat");

                Process p = new Process();
                p.StartInfo.FileName = exePath;
                string inputArg = decompressInputDlg.FileName;
                string format = ((ListBoxItem)decompressOutputFileFormatList.SelectedItem).Content.ToString();
                string outputArg = decompressOutputDlg.SelectedPath + "\\%03d." + format;
                p.StartInfo.Arguments = String.Format("\"{0}\" \"{1}\"", inputArg, outputArg);
    
                // Redirect the output stream of the child process.
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardError = true;
                StringBuilder consoleError = new StringBuilder();
                p.ErrorDataReceived += (innerSender, args) => consoleError.AppendLine(args.Data);

                p.Start();

                p.BeginErrorReadLine();
                p.WaitForExit();

                new LogForm(consoleError.ToString()).ShowDialog();
            }
        }


        private void decompressInputButton_Click(object sender, RoutedEventArgs e)
        {
            if (decompressInputDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                decompressInputPathTextBox.Text = decompressInputDlg.FileName;
            }
        }

        private void compressInputButton_Click(object sender, RoutedEventArgs e)
        {
            compressInputFolderDlg.Description = "Select folder that contains images to be compressed:";
            if (compressInputFolderDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                compressInputPathTextBox.Text = compressInputFolderDlg.SelectedPath;
            }
        }
    }
}
